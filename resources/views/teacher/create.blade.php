@extends('layout.main')

@section('title', 'SIAK | TAMBAH PENGAJAR')

@section('content')

<div class="col-lg-12 mb-4">
    <div class="card mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
            <h6 class="m-0 font-weight-bold text-primary">TAMBAH PENGAJAR</h6>
        </div>
        <hr class="sidebar-divider my-0">
        <form action="{{route('store.teahcer')}}" method="POST">
            <div class="card-body">
                <div class="form-group">
                    <label>ID Pengajar</label>
                    <input type="text" class="form-control" name="teacher_id" placeholder="ID">
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="fullname" placeholder="Nama">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label>Nomor Tlp</label>
                    <input type="text" class="form-control" name="phone" placeholder="Nomor Tlp">
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" name="address" cols="3" rows="3"></textarea>
                </div>
                <div class="form-group">
                <label>Example select</label>
                <select multiple class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                </div>
                <div class="form-group">
                <label for="exampleFormControlSelect2">Example multiple select</label>
                <select multiple class="form-control" id="exampleFormControlSelect2">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                </div>

                <div class="form-group">
                <label for="exampleFormControlReadonly">Readonly</label>
                <input class="form-control" type="text" placeholder="Readonly input here..."
                    id="exampleFormControlReadonly" readonly>
                </div>

                <div class="form-group">
                    <hr class="sidebar-divider my-3">
                    <div class="d-flex flex-row align-items-center justify-content-center">
                        <button type="submit" class="btn btn-primary btn-sm mr-2">Sign in</button>
                        <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                        <a href="{{ route('teacher')}}" class="btn btn-primary btn-sm ml-2">Back</a>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>



@endsection
