@extends('layout.main')

@section('title', 'SIAK | PENGAJAR')

@section('content')
    <div class="col-lg-12 mb-4">
        <!-- Simple Tables -->
        <div class="card">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">DATA PENGAJAR</h6>
            <a class="m-0 float-right btn btn-success btn-sm" href="{{ route('create.teacher') }}">
                <i class="fas fa-plus"></i> Tambah Pengajar
            </a>
        </div>
        <hr class="sidebar-divider my-0">
        <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
            <div class="float-right">
                <form class="navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-sm col-sm-10 bg-light border-1 small" placeholder="Cari Data ?"
                        aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
                        <button class="btn btn-primary btn-sm" type="button">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </form>
            </div>  {{-- Search Bar --}}
            <div class="py-2">
                <div class="input-group">
                    <select id="filter_branch_id" name="branch_id" class="form-control form-control-sm" style="border-color: #3f51b5;">
                        @foreach($branches as $id => $brn_name)
                            <option {{$id == request('branch_id') ? 'selected' : '' }} value="{{$id}}">{{$brn_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div> {{-- Drop Down --}}
        </div>
        <div class="table-responsive">
            @if($message = session('message'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
            <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                <th >No</th>
                <th >ID Pengajar</th>
                <th >Nama</th>
                <th >Nomor</th>
                <th >Email</th>
                <th >Mapel</th>
                <th >Pilihan</th>
                </tr>
            </thead>
            <tbody>
            @if(count($teachers) != 0)
            @foreach($teachers as $index => $teacher)
            <tr>
                <td>{{$index + $teachers->firstItem() }}</td>
                <td>{{$teacher->teacher_id}}</td>
                <td>{{$teacher->fullname}}</td>
                <td>{{$teacher->phone}}</td>
                <td>{{$teacher->email}}</td>
                <td>{{$teacher->subject->name}}</td>
                <td>
                    <div class="btn-group btn-group-sm" role="group" aria-label="...">
                        <a href="#" class="btn btn-success btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="{{ route('show.teacher', $teacher->id)}}" class="btn btn-info btn-sm">
                            <i class="fas fa-info-circle"></i>
                        </a>
                        <a href="{{route('delete.teacher', $teacher->id)}}" class="btn btn-danger btn-sm btn-delete">
                            <i class="fas fa-trash"></i>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
                <form id="form-delete" method="POST" style="display: none">
                    @csrf
                    @method('DELETE')
                </form>

            @else
            <tr>
                <td></td>
                <td></td>
                <td colspan="2" align="center">Tidak Ada Data</td>
                <td></td>
                <td></td>

            </tr>
            @endif
            </tbody>
            </table>
            <hr class="sidebar-divider my-0">
            <nav class="mt-4 d-flex justify-content-center">
                {{$teachers->appends(request()->only('branch_id'))->links()}}
            </nav>
        </div>
    </div>

@endsection


@section('scripts')
<script>
    document.getElementById('filter_branch_id').addEventListener('change', function()
    {
        let branchId = this.value || this.options[this.selectedIndex].value
        window.location.href = window.location.href.split('?')[0] + '?branch_id=' + branchId
    })


    document.querySelectorAll('.btn-delete').forEach((button) => {
        button.addEventListener('click', function(event) {
            event.preventDefault()
            if (confirm("Are You Sure ?")) {
                let action = this.getAttribute('href')
                let form = document.getElementById('form-delete')
                form.setAttribute('action', action)
                form.submit()
            }
        })
    })
</script>
@endsection