@extends('layout.main')

@section('title', 'SIAK | DETAIL PENGAJAR')


@section('content')
      <!-- General Element -->
      <div class="card mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
          <h6 class="m-0 font-weight-bold text-primary">DETAIL PENGAJAR</h6>
        </div>
        <hr class="sidebar-divider my-0">
        <div class="card-body">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-md-3">ID Pengajar :</label>
                        <div class="col-md-5">
                          <p class="form-control-plaintext text-muted">{{$teacher->teacher_id}}</p>
                        </div>
                    </div>

                    <div class="form-group row">
                      <label for="first_name" class="col-md-3">Nama :</label>
                      <div class="col-md-9">
                        <p class="form-control-plaintext text-muted">{{$teacher->fullname}}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3">Email :</label>
                      <div class="col-md-9">
                        <p class="form-control-plaintext text-muted">{{$teacher->email}}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3">Nomor Tlp :</label>
                      <div class="col-md-9">
                        <p class="form-control-plaintext text-muted">{{$teacher->phone}}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3">Alamat :</label>
                      <div class="col-md-9">
                        <p class="form-control-plaintext text-muted">{{$teacher->address}}</p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 ">Mata Pelajaran :</label>
                      <div class="col-md-9">
                        <p class="form-control-plaintext text-muted">{{$teacher->subject->name}}</p>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 ">Cabang :</label>
                      <div class="col-md-9">
                        <p class="form-control-plaintext text-muted">{{$teacher->branch->name}}</p>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row mb-0">
                      <div class="col-md-9 offset-md-3">
                        <a href="{{route('teacher')}}" class="btn btn-secondary btn-sm">Kembali</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>






@endsection