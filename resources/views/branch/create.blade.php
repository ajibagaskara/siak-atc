@extends('layout.main')

@if(isset($branch))
  @section('title', 'SIAK | EDIT CABANG')
@else
  @section('title', 'SIAK | TAMBAH CABANG')
@endif

@section('content')

<div class="col-lg-8 mb-4 mx-auto">
    <div class="card mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
          <h6 class="m-0 font-weight-bold text-primary">
            {{isset($branch) ? 'EDIT CABANG' : 'TAMBAH CABANG'}}
          </h6>
        </div>
        <hr class="sidebar-divider my-0">
        <div class="card-body">
          @if($errors->any())
          <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
              {{$error}}
            @endforeach
          </div>
          @endif
          <form action="{{ isset($branch) ? route('branch.update', $branch->id) : route('branch.store')}}" method="POST">
            @csrf
            @if(isset($branch))
              @method('PUT')
            @endif
            <div class="form-group">
              <input type="text" class="form-control" name="name" placeholder="Nama...."
              value="{{isset($branch) ? $branch->name : ''}}">
            </div>
            <hr class="sidebar-divider my-3">
            <button type="submit" class="btn btn-primary btn-sm">
              {{isset($branch) ? 'Perbarui' : 'Simpan'}}
            </button>
            <a href="{{ route('branch.index')}}" class="btn btn-primary btn-sm">Back</a>
            </div>
          </form>
        </div>
      </div>

</div>







@endsection