@extends('layout.main')

@if(isset($subject))
  @section('title', 'SIAK | EDIT MATA PELAJARAN')
@else
  @section('title', 'SIAK | TAMBAH MATA PELAJARAN')
@endif

@section('content')

<div class="col-lg-8 mb-4 mx-auto">
    <div class="card mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
          <h6 class="m-0 font-weight-bold text-primary" >
            {{isset($subject) ? 'EDIT MATA PELAJARAN' : 'TAMBAH MATA PELAJARAN'}}
          </h6>
        </div>
        <hr class="sidebar-divider my-0">
        <div class="card-body">
          @if($errors->any())
          <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
              {{$error}}
            @endforeach
          </div>
          @endif
          <form action="{{isset($subject) ? route('subject.update', $subject->id) : route('subject.store')}}" method="POST">
            @csrf
            @if(isset($subject))
              @method('PUT')
            @endif
            <div class="form-group">
              <input type="text" class="form-control" name="name" placeholder="Nama...."
              value="{{isset($subject) ? $subject->name : ''}}">
            </div>
            <hr class="sidebar-divider my-3">
            <button type="submit" class="btn btn-primary btn-sm">
              {{isset($subject) ? 'Perbarui' : 'Simpan'}}
            </button>
            <a href="{{ route('subject.index')}}" class="btn btn-primary btn-sm">Kembali</a>
            </div>
          </form>
        </div>
      </div>

</div>







@endsection