<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subject;
use App\Branch;

class Teacher extends Model
{

    protected $fillable = [
        'teacher_id', 'fullname', 'email', 'phone', 'address',
    ];

    //relationship method
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

}
