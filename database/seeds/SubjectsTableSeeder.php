<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [];

        $faker = Faker::create();

        for($i = 0; $i < 10; $i++)
        {
            $subjects[] = [
                'subj_nm' => $faker->name,
                'created_at' => now(),
            ];
        }

        DB::table('subjects')->insert($subjects);
    }
}
