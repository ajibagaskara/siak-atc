<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teacher;
use App\Subject;
use App\Branch;
use Faker\Generator as Faker;

$factory->define(Teacher::class, function (Faker $faker) {
    return [
        //insert faker data
        'teacher_id' => $faker->randomDigit,
        'fullname' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->tollFreePhoneNumber,
        'address' => $faker->address,
        'subject_id' => Subject::pluck('id')->random(),
        'branch_id' => Branch::pluck('id')->random()
    ];
});
